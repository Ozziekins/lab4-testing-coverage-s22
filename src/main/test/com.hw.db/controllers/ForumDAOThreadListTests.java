package com.hw.db.controllers;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ForumDAOThreadListTests {

    private final JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    private final ForumDAO forum = new ForumDAO(mockJdbc);
    private final ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();


    @Test
    @DisplayName("User gets list of threads test #1")
    void ThreadListTest1() {
        ForumDAO.ThreadList("slug", null, null, null);
        verify(mockJdbc).query(
                Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created;"),
                Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class)
        );
    }
}